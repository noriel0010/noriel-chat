export const CHAT_ENDPOINT = 'http://localhost:5000';
export const JOIN_EVENT = 'join';
export const MESSAGE_EVENT = 'message';
export const SEND_MESSAGE_EVENT = 'send_message';
