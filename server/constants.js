const CONNECT_EVENT = 'connect';
const DISCONNECT_EVENT = 'disconnect';
const ROOM_DATA = 'room_data';
const JOIN_EVENT = 'join';
const MESSAGE_EVENT = 'message';
const SEND_MESSAGE_EVENT = 'send_message';

module.exports = { CONNECT_EVENT, DISCONNECT_EVENT, ROOM_DATA, JOIN_EVENT, MESSAGE_EVENT, SEND_MESSAGE_EVENT };