const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const cors = require('cors');

const { addUser, removeUser, getUser, getUsersInRoom } = require('./users');

const router = require('./router');
const { CONNECT_EVENT, DISCONNECT_EVENT, ROOM_DATA, JOIN_EVENT, MESSAGE_EVENT, SEND_MESSAGE_EVENT } = require('./constants');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

app.use(cors());
app.use(router);

io.on(CONNECT_EVENT, (socket) => {
  socket.on(JOIN_EVENT, ({ name, room }, callback) => {
    const { error, user } = addUser({ id: socket.id, name, room });

    if(error) return callback(error);

    socket.join(user.room);
    socket.emit(MESSAGE_EVENT, { user: 'server', text: `${user.name}, welcome to room ${user.room}.`});
    socket.broadcast.to(user.room).emit(MESSAGE_EVENT, { user: 'server', text: `${user.name} has joined!` });

    io.to(user.room).emit(ROOM_DATA, { room: user.room, users: getUsersInRoom(user.room) });

    callback();
  });

  socket.on(SEND_MESSAGE_EVENT, (message, callback) => {
    const user = getUser(socket.id);

    io.to(user.room).emit(MESSAGE_EVENT, { user: user.name, text: message });

    callback();
  });

  socket.on(DISCONNECT_EVENT, () => {
    const user = removeUser(socket.id);

    if(user) {
      io.to(user.room).emit(MESSAGE_EVENT, { user: 'server', text: `${user.name} has left.` });
      io.to(user.room).emit(ROOM_DATA, { room: user.room, users: getUsersInRoom(user.room)});
    }
  })
});

server.listen(process.env.PORT || 5000, () => console.log(`Server has started.`));