# Realtime Chat Application

`cd server/` <br/>
`npm install` <br/>
`npm run start` <br/>
by default it will start at _http://localhost:5000_

`cd ../client` <br/>
`npm install` <br/>
(pls check if **/src/common/constants.js** is pointing to server setup (which initially is _http://localhost:5000_)) <br/>
`npm run start`
